
// userRoutes/js//

const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('',UserService.getUsers,responseMiddleware)
router.get('/:id',UserService.search,responseMiddleware)
router.post('/', createUserValid ,UserService.createUser,responseMiddleware)
router.put('/:id',updateUserValid,UserService.updateUser,responseMiddleware)
router.delete('/:id',UserService.deleteUser,responseMiddleware)



module.exports = router;

/*
 -- create user

    "firstName":"qweqwe",
    "lastName":"Verticj",
    "email":"11212k@gmail.com",
    "phoneNumber":"+380948954739",
    "password":"223"
}
 */