const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    createFighter(data,res,next){
        if (!data.body.hasOwnProperty('health')){
            data.body.health = 100
        }
        const item = FighterRepository.create(data.body)
        if(!item) {
            return res.status(400).json({error:true,message:"Error while creating "});
        }else {
            data.data = item
            next()
        }

    }
    getFighters(data,res,next){
        const item = FighterRepository.getAll();
        if(!item) {
            return res.status(404).json({error:true,message:"No fighters "});
        }else {
            data.data = item
            next()
        }
    }
    search(data,res,next) {
        const id = data.params.id
        const item = FighterRepository.getOne(id);
        if(!item) {
            return res.status(404).json({error:true,message:"Fighter Not found "});
        }else{
            data.data = item
            next()
        }

    }
    updateFighter(data,res,next){
        const id = data.params.id
        const item = FighterRepository.update(id,data)
        if(!item) {
            return res.status(400).json({error:true,message:"Error update"});
        }else{
            data.data = item
            next()
        }

    }
    deleteFighter(data,res,next){
        const id = data.params.id
        const item = FighterRepository.delete(id)
        if(!item) {
            return res.status(404).json({error:true,message:"Fighter Not found"});
        }else{
            data.data = item
            next()
        }
    }

}

module.exports = new FighterService();