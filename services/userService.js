const { UserRepository } = require('../repositories/userRepository');

class UserService {
    createUser(data,res,next){
        const item = UserRepository.create(data.body)
        if(!item) {
            return res.status(400).json({error:true,message:"Creating Error"});
        }else {
            data.data = item
            next()
        }
    }
    getUsers(data,res,next){
        const item = UserRepository.getAll();
        if(!item) {
            return res.status(404).json({error:true,message:"Not found users"});
        }else {
            data.data = item
            next()
        }
    }
    search(data,res,next) {
        const id = data.params.id
        const item = UserRepository.getOne(id);
        if(!item) {
            return res.status(404).json({error:true,message:"Not found user"});
        }else {
            data.data = item
            next()
        }
    }
    updateUser(data,res,next){
        const id = data.params.id
        const item = UserRepository.update(id,data)
        if(!item) {
            return res.status(400).json({error:true,message:"Updating Error"});
        }else {
            data.data = item
            next()
        }

    }
    deleteUser(data,res,next){
        const id = data.params.id
        const item = UserRepository.delete(id)
        if(!item) {
            return res.status(400).json({error:true,message:"Error delete"});
        }else {
            data.data = item
            next()
        }
    }
}

module.exports = new UserService();