const UserService = require('./userService');

class AuthService {
    login(userData,res) {
        const user = UserService.search(userData);
        if(!user) {
            throw Error('User not found');
        }
        res.status(200).json({user:user})
        return user;
    }
}

module.exports = new AuthService();