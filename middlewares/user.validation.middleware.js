const { user } = require('../models/user');
const { UserRepository } = require('../repositories/userRepository');
const createUserValid = (req, res, next) => {
    const {firstName, lastName,email,phoneNumber,password} = req.body
    const emailCheck = /^([A-Za-z0-9_\-\.])+\@gmail+\.([A-Za-z]{2,4})$/;
    const phoneCheck = /(\+380+[0-9]{9})$/;
    const item = UserRepository.getAll();
    let chek = true

    try {

        Object.keys(req.body).forEach((key) => {
            if(!user.hasOwnProperty(key)) {
                res.status(400).json({error: true, message: "Uncorrect arrtibutes"})
                chek = false
            }
        })

        Object.keys(user).forEach((key) => {
            if(!req.body.hasOwnProperty(key) && key !== "id") {
                res.status(400).json({error: true, message: "Uncorrect arrtibutes"})
                chek = false
            }
        })
        if(firstName === null || lastName === null){
            res.status(400).json({error: true, message: "Uncorrect FistName or LastName"})
        }
        if (!chek) {
            return
        }
        if (!emailCheck.test(email)) {
            res.status(400).json({error: true, message: "Uncorrect email"})

            return
        }
        if (!phoneCheck.test(phoneNumber)) {
            res.status(400).json({error: true, message: "Uncorrect phone"})
            return
        }
        if (password === null || !(typeof(password) === "string") ||(password.length <= 2)  ) {
            res.status(400).json({error: true, message: "Uncorrect password"})
            return
        }
        item.forEach((users) => {
            if (users["email"] === email) {
                res.status(400).json({error: true, message: "Email is used"})
                chek = false
            }
            if (users["phoneNumber"] === phoneNumber) {
                res.status(400).json({error: true, message: "Phone Number is used"})
                chek = false
            }
        });
    }catch(e){

    }
    if(chek){
        next();
    }

}

const updateUserValid = (req, res, next) => {
    const {email, phoneNumber, password} = req.body
    const emailCheck = /^([A-Za-z0-9_\-\.])+\@gmail+\.([A-Za-z]{2,4})$/;
    const phoneCheck = /(\+380+[0-9]{9})$/;
    const item = UserRepository.getAll();
    let check = true

    try {


        if (!emailCheck.test(email)) {
            res.status(400).json({error: true, message: "Uncorrect email"})
            return
        }
        if (!phoneCheck.test(phoneNumber)) {
            res.status(400).json({error: true, message: "Uncorrect phone"})
            return
        }
        if (password === null || (password.length <= 2)) {
            res.status(400).json({error: true, message: "Uncorrect phone"})
            return
        }
        item.forEach((user) => {
            if (user["id"] !== req.params.id) {
                if (user["email"] === email) {
                    res.status(400).json({error: true, message: "Email is used"})
                    check = false
                }
                if (user["phoneNumber"] === phoneNumber) {
                    res.status(400).json({error: true, message: "Phone Number is used"})
                    check = false
                }
            }
        });
    }catch(e){

    }
    if(check){
        next();
    }
}




exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;